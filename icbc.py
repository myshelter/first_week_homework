mdict = [
    {'name':'zhangsan', 'yue':100.12},
    {'name':'lisi', 'yue':10.02},
    {'name':'wangwu', 'yue':200.15},
    {'name':'zhaoliu', 'yue':900.45},
    {'name':'qianqi', 'yue':300.32}]
print('欢迎来到爱存不存银行！')

def showF():
    '''
    显示首页
    '''
    print('='*8, '银行账户管理系统', '='*8)
    print('|{0:<6}|{1:<4}|{2:<4}|{3:<4}|'.format("1.查询余额", '2.取钱', '3.存钱', '4.退出'))
    
def showB(mdict):
    '''
    显示余额
    '''
    if len(mdict) == 0:
        print('='*8, '没有账户信息可以输出！', '='*8)
    print('|{0:<2}|{1:<8}|{2:<20}'.format('id', '姓名', '余额'))
    print('-'*27)
    for i in range(len(mdict)):
        print('|{0:<2}|{1:10}|{2:<20}'.format(i+1, mdict[i]['name'], mdict[i]['yue']))

while True:
    if input('请输入你的密码：') != '123456':
        print('你输入的密码错误！')
    else:
        break
showF()
key = input('你要干嘛：')
while True:
    if key == '1':
        print('='*8, '查询账户信息', '='*8)
        showB(mdict)
        input('按任意键继续......')
    elif key == '2':
        print('='*8, '取钱', '='*8)
        showB(mdict)
        sid = input('请输入你要了取钱的账户id号：')
        money = input('请输入你要存的金额：')
        if mdict[int(sid)-1]['yue'] - int(money) >= 0:
            mdict[int(sid)-1]['yue'] -= int(money)
        else:
            print('该账户余额不足！')
        showB(mdict)
        input('按任意键继续......')
    elif key == '3':
        print('='*8, '存钱', '='*8)
        showB(mdict)
        sid = input('请输入你要存钱的账户id号：')
        money = input('请输入你要存入的金额：')
        mdict[int(sid)-1]['yue'] += int(money)
        showB(mdict)
        input('按任意键继续......')
    elif key == '4':
        print('='*13, '你已经退出！', '='*13)
        break
    else:
        print('='*13, '你的选择有误，请重新选择', '='*13)
        input('按任意键继续......')
    showF()
    key = input('你要干嘛：')

        
    
