

for i in range(1,10):
    j = i + 1
    for k in range(1, j):
        print("{}*{}={:<2}".format(k, i, i*k), end = ' ')
    print()

print('='*62)

for i in range(9, 0, -1):
    j = i + 1
    for k in range(1, j):
        print("{}*{}={:<2}".format(k, i, i*k), end = ' ')
    print()

print('='*62)

i = 1
while i <= 9:
    j = 1
    while j <= i:
        print("{}*{}={:<2}".format(j, i, i * j), end = ' ')
        j += 1
    i += 1
    print()

print('='*62)

i = 9
while i >= 1:
    j = 1
    while j <= i:
        print("{}*{}={:<2}".format(j, i, i*j), end = ' ')
        j += 1
    i -= 1
    print()

print('='*62)

for j in range(1,10):
    print('       '*(9-j), end = '')
    for i in range(j, 0, -1): 
        print("{}*{}={:<2}".format(i, j, i*j), end = ' ')
    print()

print('='*62)

for i in range(9, 0, -1):
    print("       "*(9-i), end = '')
    for j in range(i, 0, -1):
        print("{}*{}={:<2}".format(j, i, j*i), end = ' ')
    print()

print('='*62)

i = 1
while i <=9 :
    print('       '*(9-i), end = '')
    j = 1
    while j <= i:
        print('{}*{}={:<2}'.format(j, i, j*i), end = ' ')
        j += 1
    i += 1
    print()

print('='*62)

i = 9
while i >= 1:
    print('       '*(9-i), end = '')
    j = 1
    while j <= i:
        print('{}*{}={:<2}'.format(j, i, j*i), end = ' ')
        j += 1
    i -= 1
    print()
